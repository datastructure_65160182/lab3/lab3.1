/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab3__1;

/**
 *
 * @author PT
 */
public class Sqrt {

    public static int mySqrt(int x) {
        if (x == 0 || x == 1) {
            return x;
        }
        
        long left = 1;
        long right = x / 2;
        while (left <= right) {
            long mid = left + (right - left) / 2;
            long square = mid * mid;
            
            if (square == x) {
                return (int) mid;
            } else if (square < x) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        
        return (int) right;
    }

    public static void main(String[] args) {
        // EX.1
        System.out.println("EX.1");
        System.out.println("Input : 4");
        int x1 = 4;
        System.out.println("Output: " + mySqrt(x1));  // Output: 2
        // EX.2
        System.out.println("EX.2");
        System.out.println("Input : 8");
        int x2 = 8;
        System.out.println("Output: " + mySqrt(x2));  // Output: 2
    }
}
